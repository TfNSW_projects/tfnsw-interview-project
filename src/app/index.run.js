(function() {
  'use strict';

  angular
    .module('tripPlanner')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();

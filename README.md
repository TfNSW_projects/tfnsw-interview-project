# Bus Report App Angular Test

Using AngularJS, create a Bus Report page in the given skeleton application (attached) to display bus data provided.

As an example, this is one way a simple solution might look:
![Bus Report Example](bus-report-example.PNG "Bus Report Example")

## Requirements
  - On a single page, make all the data found in the busDataService accessible to the user.
  - Data can be displayed in a list or a table, or a combination of both.
  - Only the names of the organisation should be shown initially. When the user clicks on the name of the organisation,
    this should toggle the report showing the data for that organisation.
  - The first three numbers of the route variant are most important, so they should be bolded.
  - Display the following bus statuses based on its deviation from timetable - "On Time", "Late", "Early", or "Unknown".
  - Use colors of your choice to signify the status of the buses (e.g. green text might mean that the bus was on-time)
  - You may make any reasonable assumptions in your solution, but ensure that you note these assumptions down in assumptions.txt.
  - Ensure that the view is robust and is well tested.
  - You should create a private fork of the project in Bit Bucket and commit your work there
  - When you are finished, send us the details to access your fork


## Notes
  - NPM package.json and Bower bower.json is available for use.  You may install additional packages through npm or bower if required.
  - Gulp, Bootstrap, LESS CSS preprocessor, Karma/Jasmine is provided pre-configured, you may or may not choose to use them.
  - The solution needs to work on Google Chrome.
  - The requirements may be ambiguous, make sure you document any assumptions used.


## Bonus Points (Optional)
  - You will score bonus points for creativity and/or making the application look nice.
  - Provide a facility to leave notes about each organisation. There is no provided endpoint for the notes form
    submission. The data do not need to be saved to a server/database.


## Getting Started
To start, run 'npm install' and 'bower install'. This will install the project dependencies.  Then run 'gulp' on the command line. This will open a browser window at http://localhost:3000/#/  For some reason, this code doesn't work on node 7.  Recommended versions are node (6.9.1) and bower (1.7.9).


Good luck!
